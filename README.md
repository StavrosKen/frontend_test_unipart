From this directory run `npm install` then `npm start`. Visit localhosts:3000

  This is a `create-react-app` seeded React application that uses MobX and Sass.
  It is configured from a .json file and this is where you can change/expand the app.
  This configuration hydrates the Mobx model which then acts as single source of truth to all the components.

  The emphasis on this metadata driven app was given on extendability:

  The tabs completed are 1 and 2. 
  By changing the metadata.json file though, one can easily fill in the rest of the tabs in a matter of seconds.
  Note that for any config changes in the metadata.json you need to make sure your localstorage "reports" entry doesnt exist. If it does, delete it or press 'Reset all' from the UI.
  This will re-create the model according to the metadata.json structure.

  The design also allows for multiple reports to be displayed even simultaneously. To test that, add a new report in the metadata.json and include an 'IncidentReport' component with its id in the 'IncidentLogistics' scene.

  We can have multiple models, since store.js bundles them in an object and 'Provider' destructures them.
"



Q0.
--
1000 kg of berries are initially found to be 99% water.
After a week, due to evaporation, the berries are 98% water.
What is the mass of the berries now?

Answer: "500kg. Since the 1% (10kg) of total is non evaporable then we ll always have 10kg of berries element.
With that being said, 2% of total is 10kg."



Q2.
---
A text file contains a list of numbers, one per line.
Write a shell pipeline (i.e. a bash one-liner) which will print out the five
most commonly occurring numbers, one per line, without surrounding whitespace, in
descending order of frequency.
Use of perl, awk, or any other programming language should be avoided.

Answer: "grep . numbers.txt | sort | uniq -c | sort -k1,1nr -k2 | sed -r 's/[ ]+[0-9]+[ ]//g' | head -n 5"



Q5.
---
A videoconferencing application, running on a mobile device, is
consuming 80% CPU, and causing the battery to drain rapidly. What
steps might you take to improve battery life?

[You have the source code for the application, so you are able to
investigate/change both the application itself and its environment]

Answer "
First, I would try some different configuration in the app and benchmark with each change to find where the problem is.
For example: 
  I would lower the video quality, so the app will receive and process less packets.
  Switch from 4G to 3G to see how this affects the app. (4G uses significantly more battery than 3G)
  Check the garbage collector's workload during the runtime.

Lastly, I would analyze the codebase with a profiling tool and inspect for bad code that may overuse resources.

The above should give us enough information to start targeted changes and resolve the issue.