import React from 'react';
import style from './style.module.scss';

class Header extends React.Component {

  render() {
    return (
      <header className={style.header}>
        <h1>Frontend Test Unipart</h1>
        {this.props.children}
      </header>
    );
  }
}

export default Header;
