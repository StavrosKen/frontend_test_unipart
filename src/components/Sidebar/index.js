import React from 'react';
import style from './style.module.scss';

class Sidebar extends React.Component {

  render() {
    return (
      <aside className={style.sidebar}>
        <h3>Sidebar</h3>
        {this.props.children}
      </aside>
    );
  }
}

export default Sidebar;
