import React from 'react';
import { render } from 'react-dom';
import {
    Router,
    Route,
    Switch,
} from 'react-router-dom';
import createBrowserHistory from "history/createBrowserHistory";
import { configure } from "mobx"
import { Provider } from 'mobx-react';
import store from './store';
import IncidentLogistics from './scenes/IncidentLogistics';

import './index.scss';

configure({ enforceActions: 'always' })
const history = createBrowserHistory()

render(
    <Router history={history}>
        <Provider {...store}>
            <Switch>
                <Route path="/" component={IncidentLogistics} />
            </Switch>
        </Provider>
    </Router>,
    document.getElementById('root')
);
