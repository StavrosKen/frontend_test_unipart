import { observable, action } from "mobx";
import metadata from '../scenes/IncidentLogistics/utils/metadata.json';

const REPORTS = 'reports';

class Reports {
    @observable
    reports = localStorage.getItem(REPORTS) ? JSON.parse(localStorage.getItem(REPORTS)) : this.copyMetaData();

    @action
    updateValue(reportId, inputName, value) {
        this.reports.forEach(report => {
            if (report.id === reportId) {
                report['form-steps'].forEach(step => {
                    step.inputs.forEach(input => {
                        if (input.title === inputName) input.value = value
                        if (input.optionalFields) {
                            input.optionalFields.forEach(field => {
                                if (field.title === inputName) field.value = value
                            });
                        }
                    });
                });
            }
        });
    }

    @action
    updateActiveStep(reportId, step) {
        this.reports.forEach(report => {
            if (report.id === reportId) {
                report['active-step'] = step;
            }
        });
    }

    @action
    resetAll() {
        this.reports = this.copyMetaData();
        localStorage.removeItem(REPORTS);
    }

    @action
    resetStep(reportId, stepNumber) {
        this.reports.forEach(report => {
            if (report.id === reportId) {
                report['form-steps'].forEach((step, index) => {
                    if (index === stepNumber) step.inputs.forEach(input => input.value = '')
                });
            }
        });
        this.saveStep(reportId, stepNumber);
    }

    @action
    completeReport(reportId) {
        this.reports.forEach(report => {
            if (report.id === reportId) {
                if (this.reportProgress(reportId) === 100) {
                    report.completed = true;
                    return true;
                }
                return false;
            }
        });

        this.saveAll();
    }

    reportProgress(reportId, stepNumber = null) {
        let filledInputs = 0;
        let totalInputs = 0;
        this.reports.filter(report => report.id === reportId)[0]['form-steps']
            .forEach((step, index) => {
                if (stepNumber === null || index === stepNumber) {
                    step.inputs.forEach(input => {
                        if (!input.optional && input.value !== '') filledInputs++
                        if (!input.optional) totalInputs++;
                        const optionalButTrue = input.optional && input.value;
                        if (optionalButTrue) input.optionalFields.forEach(field => {
                            if (field.value !== '') filledInputs++
                            totalInputs++;
                        })
                    });
                }
            });
        const progress = Number((filledInputs * 100 / totalInputs).toFixed(0));
        return progress;
    }


    saveAll() {
        localStorage.setItem(REPORTS, JSON.stringify(this.reports));
    }

    saveStep(reportId, stepNumber) {
        const newStep = this.reports.filter(report => report.id === reportId)[0]['form-steps'].filter((step, index) => index === stepNumber)[0];
        let savedReports = localStorage.getItem(REPORTS) ? JSON.parse(localStorage.getItem(REPORTS)) : this.copyMetaData();

        savedReports.forEach(report => {
            if (report.id === reportId) {
                report['form-steps'].splice(stepNumber, 1, newStep);
            }
        });

        localStorage.setItem(REPORTS, JSON.stringify(savedReports));
    }

    copyMetaData() {
        return JSON.parse(JSON.stringify(metadata.reports));
    }
}

export default new Reports();