
import React, { Component } from 'react';
import { observer, inject } from "mobx-react"
import { Button } from "react-bootstrap"

@inject('reports')
@observer
class Controls extends Component {

  updateStep = movement => {
    const report = this.props.report;
    this.props.reports.updateActiveStep(report.id, report['active-step'] + movement);
  }

  saveStep = () => {
    if (window.confirm("Are you sure you want to save this step's progress?")) {
      this.props.reports.saveStep(this.props.report.id, this.props.stepIndex);
    }
  }

  resetStep = () => {
    if (window.confirm("Are you sure you want to reset this step's progress?")) {
      this.props.reports.resetStep(this.props.report.id, this.props.stepIndex);
    }
  }

  render() {
    const report = this.props.report;
    
    return (
        <div>
            <Button onClick={this.saveStep} bsStyle="success">Save</Button>
            <Button onClick={this.resetStep} bsStyle="danger">Reset</Button>
            <Button onClick={() => this.updateStep(-1)} disabled={report['active-step'] === 1}>Back</Button>
            <Button onClick={() => this.updateStep(1)} disabled={report['active-step'] === report['form-steps'].length}>Next</Button>
        </div>
    );
  }
}

export default Controls;
