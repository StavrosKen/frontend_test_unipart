import React, { Component } from 'react';
import { observer, inject } from "mobx-react"
import { Button } from "react-bootstrap"

import style from './style.module.scss';

@inject("reports")
@observer
class ReportHeader extends Component {
    
    saveAll = () => {
        if (window.confirm("Are you sure you wa nt to save all progress?")) {
          this.props.reports.saveAll();
        }
      }

    resetAll= reportId => {
        if (window.confirm("Are you sure you want to reset all progress?")) {
          this.props.reports.resetAll(reportId);
        }
    }

    render() {
        const report = this.props.reports.reports.find(report => report.id === this.props.reportId);

        return (
            <div>
                <h2 className={style.reportTitle}>{report.title}</h2>
                <Button className={style.completeReportButton} onClick={() => this.props.reports.completeReport(report.id)}>{report.completed ? 'Completed': 'Complete'}</Button>
                <Button className={style.resetAllButton} onClick={() => this.resetAll(report.id)} bsStyle="danger">Reset All</Button>
                <Button className={style.saveAllButton} onClick={this.saveAll} bsStyle="success">Save All</Button>
            </div>
        );
    }
}

export default ReportHeader;
