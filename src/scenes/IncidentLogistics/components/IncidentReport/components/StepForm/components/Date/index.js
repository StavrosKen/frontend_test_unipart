import React from 'react';
import { observer } from "mobx-react"
import { Col } from "react-bootstrap"
import { DatePicker, DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';

import style from './style.module.scss';

@observer
class Date extends React.Component {

    change = (jsDate, dateString) => {
        this.props.updateValue(dateString);
    }

    render() {
        const input = this.props.input;

        return (
            <Col xs={12} sm={6} className="form-element">
                <label htmlFor={input.title}>{input.title}</label>
                <DatePickerInput id={input.title} onChange={this.change} value={input.value} />
                <DatePicker className={style.datePicker} onChange={this.change} value={input.value} />
            </Col>
        );
    }
}

export default Date;