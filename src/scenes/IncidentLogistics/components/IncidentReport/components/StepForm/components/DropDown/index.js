import React from 'react';
import { Col, DropdownButton, MenuItem } from 'react-bootstrap';
import { observer } from "mobx-react"

@observer
class DropDown extends React.Component {

    render() {
        const input = this.props.input;

        return (
            <Col xs={12} sm={6} className="form-element">
                <label htmlFor={input.title}>{input.title}</label>
                <DropdownButton bsStyle="primary" title={input.value || 'Please select'} id={input.title} onSelect={this.props.updateValue}>
                    {input.options.map((option, i) => {
                        return <MenuItem eventKey={option.value} key={i} >{option.label}</MenuItem>
                    })}
                </DropdownButton>
            </Col>
        );
    }
}

export default DropDown;