import React from 'react';
import { Col, ToggleButtonGroup, ToggleButton } from 'react-bootstrap';
import { observer } from "mobx-react"

@observer
class Radio extends React.Component {

    render() {
        const input = this.props.input;

        return (
            <Col xs={12} sm={6} className="form-element">
                <ToggleButtonGroup type="radio" name="options" defaultValue={input.value} onChange={this.props.updateValue}>
                    {input.options.map((option, i) => <ToggleButton value={option.value} key={i}>{option.label}</ToggleButton>)}
                </ToggleButtonGroup>
            </Col>
        );
    }
}

export default Radio;