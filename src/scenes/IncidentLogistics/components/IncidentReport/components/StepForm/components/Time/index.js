import React from 'react';
import { observer } from "mobx-react"
import { Col, DropdownButton, MenuItem } from 'react-bootstrap';

@observer
class Date extends React.Component {

    change = (type, value, otherTypeValue) => {
        let time;
        if (type === 'hours') {
            time = `${value}:${otherTypeValue || 0}`;
        } else {
            time = `${otherTypeValue || 0}:${value}`;
        }
        this.props.updateValue(time);
    }

    render() {
        const input = this.props.input;
        const value = this.props.input.value.split(':');
        const hours = value.length && value.length > 1 ? value[0] : null;
        const minutes = value.length && value.length > 1 ? value[1] : null;


        return (
            <Col xs={12} sm={6} className="form-element">
                <label htmlFor={input.title}>{input.title}</label>
                <DropdownButton bsStyle="primary" title={hours || 'Please select'} id={input.title} onSelect={value => this.change('hours', value, minutes)}>
                    {Array.from(Array(24).keys()).map((option, i) => {
                        return <MenuItem eventKey={option} key={i}>{option}</MenuItem>
                    })}
                </DropdownButton>
                <DropdownButton bsStyle="primary" title={minutes || 'Please select'} id={input.title} onSelect={value => this.change('minutes', value, hours)}>
                    {Array.from(Array(59).keys()).map((option, i) => {
                        return <MenuItem eventKey={option} key={i}>{option}</MenuItem>
                    })}
                </DropdownButton>
            </Col>
        );
    }
}

export default Date;