import React from 'react';
import { observer, inject } from "mobx-react"
import { Row, Col } from "react-bootstrap"
import Toggle from 'react-toggle'
import DropDown from './components/DropDown';
import Date from './components/Date';
import Time from './components/Time';
import Radio from './components/Radio';

import './styles.scss';

@inject("reports")
@observer
class StepForm extends React.Component {

    updateInputValue = (inputName, value) => {
        this.props.reports.updateValue(this.props.report.id, inputName, value);
    }

    generateFormInputs = inputs => {
        return inputs.map((input, i) => {
            switch (input.type) {
                case 'date':
                    return <Date updateValue={value => this.updateInputValue(input.title, value)} input={input} key={i} />;

                case 'time':
                    return <Time updateValue={value => this.updateInputValue(input.title, value)} input={input} key={i} />;

                case 'dropdown':
                    return <DropDown updateValue={value => this.updateInputValue(input.title, value)} input={input} key={i} />;

                case 'radio':
                    return <Radio updateValue={value => this.updateInputValue(input.title, value)} input={input} key={i} />;

                case 'switch':
                    return (
                        <Col xs={12} sm={6} key={i} className="form-element">
                            <label htmlFor={input.title}>{input.title}</label>
                            <Toggle defaultChecked={input.value || input.default} onChange={e => this.updateInputValue(input.title, e.target.checked)} />
                            {input.optional && input.value === true && this.generateFormInputs(input.optionalFields)}
                        </Col>
                    );

                case 'textarea':
                    return (
                        <Col xs={12} sm={6}key={i} className="form-element">
                            <label htmlFor={input.title}>{input.title}</label>
                            <textarea id={input.title} value={input.value} onChange={e => this.updateInputValue(input.title, e.target.value)}></textarea>
                        </Col>
                    );

                default:
                    return (
                        <Col xs={12} sm={6} key={i} className="form-element">
                            <label htmlFor={input.title}>{input.title}</label>
                            <input id={input.title} type={input.type} value={input.value} onChange={e => this.updateInputValue(input.title, e.target.value)}/>
                        </Col>
                    );
            }
        });
    }

    render() {
        const formInputs = this.generateFormInputs(this.props.step.inputs);
        const formContent = formInputs.length > 0 ?  formInputs : 'Coming Soon!';

        return (
            <div className="step-content">
                <div className="form-wrapper">
                    <h3>{this.props.step.title}</h3>
                    <Row>
                        {formContent}
                    </Row>
                </div>
            </div>
        );
    }
}

export default StepForm;
