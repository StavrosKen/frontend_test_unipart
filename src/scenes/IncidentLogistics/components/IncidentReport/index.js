import React, { Component } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import { observer, inject } from "mobx-react"
import ReportHeader from './components/ReportHeader';
import StepForm from './components/StepForm';
import Controls from './components/Controls';
import { ProgressBar } from 'react-bootstrap';

import style from './style.module.scss';

@inject("reports")
@observer
class IncidentReport extends Component {

    generateTabs = report => {
        return report['form-steps'].map((step, i) => {
            const title = this.generateTabTitle(report.id, i, step);

            return (
                <Tab eventKey={i + 1} key={i + 1} title={title}>
                    <StepForm report={report} step={step} stepIndex={i} />
                    <Controls report={report} stepIndex={i} />
                </Tab>
            );
        });
    }

    generateTabTitle = (reportId, stepNumber, step) => {
        const progress = this.props.reports.reportProgress(reportId, stepNumber);

        return (
            <div className={style.tabTitleWrapper}>
                {step.title}
                {step.inputs.length > 0 && <ProgressBar className={style.titleProgress} now={progress} />}
            </div>
        );
    }

    render() {
        const report = this.props.reports.reports.find(report => report.id === this.props.id);
        const progress = this.props.reports.reportProgress(report.id);

        return (
            <section>
                <ReportHeader reportId={report.id} />
                <Tabs id="report-tabs" className={style.reportTabs} activeKey={report['active-step']} onSelect={e => this.props.reports.updateActiveStep(report.id, e)}>
                    <ProgressBar active className={style.progress} now={progress} label={`${progress}%`} />
                    {this.generateTabs(report)}
                </Tabs>
            </section>
        );
    }
}

export default IncidentReport;
