import React, { Component } from 'react';
import Header from '../../components/Header'; // TODO: add components alias in webpack
import Sidebar from '../../components/Sidebar';
import Content from '../../components/Content';
import IncidentReport from './components/IncidentReport';


import './styles.scss';

class IncidentLogistics extends Component {

  render() {
    return (
      <React.Fragment>
        <Header />
        <Sidebar />
        <Content>
          <IncidentReport id="report-1" />
        </Content>
      </React.Fragment>
    );
  }
}

export default IncidentLogistics;
