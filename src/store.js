import reports from './models/reports';

const appStore = {
    reports,
}

export default appStore;